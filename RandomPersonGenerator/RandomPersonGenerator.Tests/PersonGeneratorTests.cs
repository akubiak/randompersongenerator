﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RandomPersonGenerator.Tests
{
    [TestClass]
    public class PersonGeneratorTests
    {
        [TestMethod]
        public void GenerateRandomPerson_should_return_not_null_object()
        {
            var result = PersonGenerator.Instance.GenerateRandomPerson();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void GenerateRandomPersonList_should_return_not_empty_list()
        {
            var result = PersonGenerator.Instance.GenerateRandomPersonList();
            Assert.IsNotNull(result);
            Assert.IsFalse(result.Count == 0);
        }

        [TestMethod]
        public void GenerateRandomPersonList_every_person_should_have_appropriate_birthday_date_to_age()
        {
            var result = PersonGenerator.Instance.GenerateRandomPersonList(1000);
            foreach(var person in result)
            {
                bool isValidBirthday = person.Age == (DateTime.Now.Year - person.Birthday.Year);
                if(!isValidBirthday)
                {

                }
                Assert.IsTrue(isValidBirthday);
            }
        }
    }
}
