﻿using RandomPersonGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomPersonGenerator.Helpers
{
    public static class GenderHelper
    {
        private static Random _random = new Random(DateTime.Now.Millisecond);
        
        /// <summary>
        /// Return random gender
        /// </summary>
        public static Gender GetRandomGender()
        {
            var result = _random.Next(0, 2);
            return (Gender)result;
        }
    }
}
