﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomPersonGenerator.Helpers
{
    public static class LastNameHelper
    {
        private static Random _random = new Random(DateTime.Now.Millisecond);
        private static List<string> _lastNames = new List<string>() { "Lee", "Crawford", "Lane", "Kelly", "Kim", "George", "Davis", "Mccoy", "Rogers", "Simmmons", "Harvey", "Ramos", "Snyder", "Armstrong", "Alexander", "Newman", "Knight", "Peters", "Oliver", "Gonzalez", "Ryan", "Welch", "Scott", "Montgomery", "Ross", "Price", "Wells", "Burton", "Williams", "Chapman", "Olson", "Howell", "Schmidt", "Willis", "Little", "Harper", "Harrison", "Diaz", "Nichols", "Austin", "Weaver", "Craig", "Myers", "Campbell", "Kuhn", "Allen", "Lambert", "Johnson", "Pena", "Byrd", "Terry", "Kelley", "Jackson", "Bell", "Sanchez", "Cook", "Watson", "Andrews", "Simpson", "Curtis", "Vasquez", "Freeman", "Carr", "Burke", "Bryant", "Williamson", "Arnold", "Fowler", "Meyer", "Jensen", "Herrera", "Graham", "Dean", "Palmer", "Hall", "Castro", "Tucker", "Cole", "Hoffman", "Stevens", "Gomez", "Taylor", "Mason", "Jimenez", "Ruiz", "King", "Obrien", "Hill", "Shaw", "Hughes", "Silva", "Dunn", "Stanley", "Powell", "Wright", "Baker", "Gilbert", "Wood", "Torres", "Bowman", "Castillo", "Hunt", "Sims", "Alvarez", "Marshall", "Vargas", "Butler", "Morris", "Riley", "Sutton", "Perry", "Lawson", "Reyes", "Elliott", "Garza", "Dixon", "Clark", "Fuller", "Sullivan", "Lynch", "Carlson", "White", "Adams", "Black", "Wade", "Davidson", "Holt", "Barnett", "Gonzales", "James", "Wallace", "Mcdonalid", "Cruz", "Chambers", "West", "Martin", "Bates", "Carter", "Ward", "Warren", "Perez", "Hamilton", "Stewart", "Horton", "Moreno", "Richardson", "Smith", "Wagner", "Lopez", "Woods", "Fisher", "Morgan", "Garcia", "Richards", "Hayes", "Hudson", "Ramirez", "Brewer", "Peterson", "Reynolds", "Howard", "Caldwell", "Thompson", "Douglas", "Rice", "Brown", "Boyd", "Garrett", "Gutierrez", "Lawrence", "Banks", "Hart", "Matthews", "Robertson", "Simmons", "Duncan", "Fields", "Harris", "Gordon", "Walker", "Murphy", "Reid", "Patterson", "Watkins", "Larson", "Robinson", "Kennedy", "Bailey", "Russell", "Franklin", "Walters", "Carpenter", "Lewis", "Bennett", "Mitchell", "Hernandez", "Nguyen", "Bradley", "Payne", "Mills", "Ortiz", "Wheeler", "Henderson", "Graves", "Hansen", "Perkins", "Miles", "Barrett", "Long", "Griffin", "Watts", "Nelson", "Webb", "Berry", "Edwards", "Mitchelle", "Moore", "Johnston", "Gibson", "Daniels", "Medina", "Pearson", "Brooks", "Murray", "Hunter", "Fernandez", "Beck", "Hawkins", "May", "Foster", "Thomas", "Lucas", "Neal", "Washington", "Bishop", "Cooper", "Martinez", "Turner", "Stephens", "Rodriquez", "Evans", "Carroll", "Cunningham", "Ray", "Lowe", "Spencer", "Gardner", "Flores", "Roberts", "Day", "Mendoza", "Burns", "Barnes", "Rhodes", "Mckinney", "Jacobs", "Ellis", "Grant", "Rodriguez", "Parker", "Romero", "Fox", "Miller", "Henry", "Reed", "Morales", "Hanson", "Hale", "Ferguson", "Sanders", "Chavez" };
        
        /// <summary>
        /// Return random last name
        /// </summary>
        public static string GetRandomLastName()
        {
            return _lastNames.ElementAt(_random.Next(_lastNames.Count));
        }
    }
}
