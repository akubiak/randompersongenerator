﻿using RandomPersonGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomPersonGenerator.Helpers
{
    public static class FirstNameHelper
    {
        private static Random _random = new Random(DateTime.Now.Millisecond);
        private static List<string> _femaleFirstNames = new List<string>() { "Beth", "Margie", "Riley", "Isabella", "Louella", "Lillian", "Kenzi", "Lily", "Susan", "Danielle", "Nellie", "Catherine", "Mildred", "Deanna", "Carla", "Grace", "Kay", "Peggy", "Zoey", "Dolores", "Christina", "Felecia", "Hilda", "Jamie", "Alicia", "Juanita", "Michelle", "Brianna", "Ella", "Janice", "Vivan", "Irma", "Zoe", "Lisa", "Janet", "Alma", "Terra", "Claire", "Sherri", "Savannah", "June", "Carmen", "Serenity", "Vera", "Anita", "Arlene", "Miriam", "Georgia", "Candice", "Mattie", "Lena", "Edna", "Irene", "Ruby", "Marie", "Sherry", "Shelly", "Jean", "Constance", "Gail", "Becky", "Jacqueline", "Elizabeth", "Lucille", "Genesis", "Denise", "Beatrice", "Lori", "Roberta", "Hailey", "Crystal", "Rhonda", "Nicole", "Toni", "Camila", "Jessie", "Sharlene", "Elsie", "Patsy", "Colleen", "Megan", "Christy", "Layla", "Sara", "Diana", "Michele", "Audrey", "Amber", "Lorraine", "Rachel", "Pamela", "Letitia", "Lucy", "Shannon", "Lydia", "Kylie", "Florence", "Emily", "Hazel", "Minnie", "Vanessa", "Bella", "Aubree", "Joyce", "Stacey", "Katie", "Norma", "Ashley", "Priscilla", "Dianne", "Alexa", "Alyssa", "Isobel", "Renee", "Kristin", "Sophia", "Melissa", "Valerie", "Brandie", "Tara", "Harper", "Felicia", "Loretta", "Brittany", "Gloria", "Deann", "Courtney", "Rita", "Evelyn", "Judy", "Doris", "Victoria", "Terri", "Claudia", "Cindy", "Wendy", "Ava", "Lynn", "Stella", "Anna", "Gertrude", "Eleanor", "Kaylee", "Erika", "Peyton", "Stephanie", "Kelly", "Marian", "Sofia", "Abigail", "Heidi", "Joanne", "Leslie", "Amy", "Kathryn", "Charlene", "Melanie", "Kim", "Rosa", "Erica", "Lesa", "Katrina", "Amanda", "Wilma", "Sally", "Erin", "Anne", "Josephine", "Katherine", "Delores", "Annie", "Brooklyn", "Chloe", "Gwendolyn", "Rosemary", "Vicki", "Myrtle", "Bonnie", "Leta", "Annette", "Gabriella", "Cassandra", "Martha", "Stacy", "Lillie", "Dora", "Misty", "Jennifer", "Addison", "Dawn", "Cherly", "Yvonne", "Debra", "Melinda", "Herminia", "Marjorie", "Aubrey", "Linda", "Marcia", "Willie", "Jill", "Eileen", "Carolyn", "Sophie", "Tonya", "Arianna", "Glenda", "Jackie", "Suzanne", "Scarlett", "Caroline", "Tammy", "Emma", "Theresa", "Meghan", "Tina", "Sandra", "Vickie", "Lois", "Gina", "Marsha", "Bernice", "Jenny", "Louise", "Olivia", "Marion", "Phyllis", "Cathy", "Jennie", "Pauline", "Tanya", "Kristen", "Alexis", "Veronica", "Jessica", "Natalie", "Sheila", "Nora", "Jane", "Sylvia", "Sue", "Kathy", "Frances", "Regina", "Carrie", "Celina", "Bobbie", "Bessie", "Beverly", "Ellen", "Bertha", "Holly", "Nevaeh", "Madison", "Paula", "Edith", "Rose", "Marlene", "Teresa", "Beverley", "Tamara", "Dana", "Brandy", "Mary", "Jeanette", "Maxine", "Wanda", "Sarah", "Tracey", "Charlotte", "Judith", "Leah", "Billie", "Connie", "Jo", "Taylor", "Rebecca", "Daisy", "Ethel", "Kitty", "Esther", "Marilyn" };
        private static List<string> _maleFirstNames = new List<string>() { "Harvey", "Brent", "Jerome", "Greg", "Lewis", "Dwight", "James", "Rafael", "Luis", "Lester", "Brett", "Charles", "Albert", "Nathaniel", "Marc", "Ernest", "Paul", "Julio", "Dylan", "Bobby", "Caleb", "Kirk", "Ritthy", "Andy", "Howard", "Jared", "Fred", "Tracy", "Leo", "Todd", "Stephen", "Joseph", "Dean", "Lance", "Herbert", "Henry", "Andrew", "Norman", "Jacob", "Bill", "Vincent", "Salvador", "Morris", "Mitchell", "Kurt", "Mark", "Rodney", "Ron", "Frederick", "Lawrence", "Brian", "Anthony", "Darren", "Phillip", "Everett", "Clayton", "Lucas", "Kenneth", "Ken", "Raul", "Reginald", "Tyler", "Don", "Isaiah", "Eric", "Mathew", "Roberto", "Ian", "Brennan", "Landon", "Nelson", "Seth", "Karl", "Logan", "Jayden", "Nathan", "Levi", "Elijah", "Bernard", "Wyatt", "Derrick", "Michael", "Dwayne", "Freddie", "Ricky", "Clyde", "Bruce", "Lee", "Virgil", "Lonnie", "Edwin", "Tom", "Jeremiah", "Leroy", "Peter", "Donald", "Alfredo", "Rene", "Leslie", "Evan", "Cody", "Javier", "Joshua", "Rick", "Corey", "Jordan", "Francis", "Ruben", "Dennis", "Cory", "David", "Wallace", "Jesus", "Terrence", "Wayne", "Byron", "Gregory", "Fernando", "Edgar", "Brayden", "Hector", "Angel", "Daryl", "Bryan", "Carlos", "Earl", "Same", "Nicholas", "Jack", "Timmothy", "Kelly", "Jose", "Pedro", "Jorge", "Justin", "Armando", "Ronald", "Marshall", "Gary", "Roy", "Miguel", "Christopher", "Shawn", "Cameron", "Robert", "Jerry", "Adam", "Jackson", "Arnold", "Noah", "Antonio", "Ivan", "Brad", "Eduardo", "Gordon", "Gavin", "Dale", "Francisco", "Duane", "Joe", "Stanley", "Tomothy", "Russell", "Curtis", "Gerald", "Alfred", "Liam", "Martin", "Melvin", "Bob", "Larry", "Zachary", "Jamie", "Eddie", "Frank", "Jason", "Manuel", "William", "Charlie", "Tyrone", "Allan", "Maurice", "Daniel", "Cecil", "Gene", "Ramon", "Richard", "Johnni", "Tim", "Carter", "Johnny", "Scott", "Juan", "Allen", "Jonathan", "Julian", "Philip", "Max", "Isaac", "Benjamin", "Neil", "Hunter", "Alvin", "Darryl", "Jessie", "Tony", "Troy", "Glen", "Jeff", "Wesley", "Raymond", "Ryan", "Willard", "Shane", "Dan", "Joel", "Matthew", "Dustin", "Steve", "Kyle", "Chester", "Travis", "Felix", "Darrell", "Austin", "Mario", "Aiden", "Billy", "Lloyd", "Harold", "Adrian", "Jar", "Randy", "Arthur", "Erik", "Wade", "Ricardo", "Sean", "Gilbert", "Micheal", "Jimmy", "Soham", "Calvin", "Andre", "Danny", "Harry", "Aaron", "Herman", "Walter", "Randall", "Marcus", "Chris", "Jeremy", "Warren", "Leonard", "Victor", "Milton", "Flenn", "Alexander", "Jon", "Arron", "Jeffrey", "Marvin", "Sebastian", "Douglas", "Alan", "Jim", "Jimmie", "George", "Steven", "Owen", "Clinton", "Christian", "Marion" };

        /// <summary>
        /// Return random first name of specific gender
        /// </summary>
        public static string GetRandomFirstName(Gender gender)
        {
            if (gender == Gender.Female)
            {
                return GetRandomFemaleName();
            }
            else
            {
                return GetRandomMaleName();
            }
        }

        private static string GetRandomFemaleName()
        {
            return _femaleFirstNames.ElementAt(_random.Next(_femaleFirstNames.Count));
        }

        private static string GetRandomMaleName()
        {
            return _maleFirstNames.ElementAt(_random.Next(_maleFirstNames.Count));
        }
    }
}
