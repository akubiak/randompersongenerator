﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomPersonGenerator.Helpers
{
    public static class DateTimeHelper
    {
        private static Random _radnom = new Random(DateTime.Now.Millisecond);

        /// <summary>
        /// Return random date, range from startDate to today
        /// </summary>
        public static DateTime GetRandomDate(DateTime startDate)
        {
            var endDate = new DateTime(startDate.Year, 12, 31);
            int range = (endDate - startDate).Days;
            return startDate.AddDays(_radnom.Next(range));
        }

        /// <summary>
        /// Return random date, range from startDate to endDate
        /// </summary>
        public static DateTime GetRandomDate(DateTime startDate, DateTime endDate)
        {
            int range = (endDate - startDate).Days;
            return startDate.AddDays(_radnom.Next(range));
        }
    }
}
