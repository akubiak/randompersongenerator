﻿using Newtonsoft.Json;
using RandomPersonGenerator.Helpers;
using RandomPersonGenerator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RandomPersonGenerator
{
    public class PersonGenerator
    {
        private Random _random;
        private PersonGenerator()
        {
            _random = new Random(DateTime.Now.Millisecond);
        }

        #region Singleton
        static private PersonGenerator _instance = null;
        public static PersonGenerator Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new PersonGenerator();
                }
                return _instance;
            }
        }
        #endregion

        /// <summary>
        /// Return random person
        /// </summary>
        public Person GenerateRandomPerson(int minAge = 18, int maxAge = 48)
        {
            Person result = new Person();
            result.Gender = GenderHelper.GetRandomGender();
            result.FirstName = FirstNameHelper.GetRandomFirstName(result.Gender);
            result.LastName = LastNameHelper.GetRandomLastName();
            result.Age = _random.Next(minAge, maxAge);
            result.Birthday = DateTimeHelper.GetRandomDate(DateTime.Now.AddYears(-result.Age));
            return result;
        }

        /// <summary>
        /// Return random person as json string
        /// </summary>
        public string GenerateRandomPersonAsJson(int n = 20, int minAge = 18, int maxAge = 48)
        {
            var result = GenerateRandomPerson(minAge, maxAge);
            return JsonConvert.SerializeObject(result);
        }

        /// <summary>
        /// Return list of random person 
        /// </summary>
        public List<Person> GenerateRandomPersonList(int n = 20, int minAge = 18, int maxAge = 48)
        {
            List<Person> result = new List<Person>();
            while (n > 0)
            {
                result.Add(GenerateRandomPerson(minAge, maxAge));
                n--;
            }
            return result;
        }

        /// <summary>
        /// Return list of random person as json string
        /// </summary>
        public string GenerateRandomPersonListAsJson(int n = 20, int minAge = 18, int maxAge = 48)
        {
            var result = GenerateRandomPersonList(n, minAge, maxAge);
            return JsonConvert.SerializeObject(result);
        }
    }
}
